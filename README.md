
Feeder
=======

The idea of this mini-tool is to:

1. catch magnet links from an RSS (where "link" contains the magnet URI);
2. push the links into a qbittorrent instance.

There is an intermediate text-file that holds the links which can be
edited for sorting.

Usage
------

First, generate some rules that describe the links you are interested in. The *dn* part
of the magnet link is used to match rules against. The rule file can contain several rules,
each is matched.

Example rules (save to rules.txt):

    * ; everything
    *720p* and *2016* ; newmovies
    Game?of?thrones* ; tvshow
    *XXX* and not *Shemale* and (*1080p* or *2160p*); badstuff


Update movies.txt with links:

    ./feeder.py --import --import-url "https://rarbg.com/rssdd_magnet.php?categories=movies" -f movies.txt

Send torrents to qbittorrent from movies.txt (those without "T" flag)

    ./feeder.py --export -f movies.txt --qb-user admin -qb-pass secret123 --max-torrents 2

(will not add new magnet links if there are more than 2 torrents in qbittorrent)
