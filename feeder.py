#!/usr/bin/env python

from __future__ import print_function
from xml.dom.minidom import parseString
import requests
import argparse, sys, re, os, fnmatch
import urlparse
import csv
import time

cleanup_rules = (
    {"find": "2160p", "remove": ["1080p", "720p"]},
    {"find": "1080p", "remove": ["720p"]},
)

db_file = None

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class QBExporter:
    """ Export torrent using QBittorrent"""
    def __init__(self, url, user, secret):
        self.QB_URL = url + "%s"
        self.cookies = self.login_(user, secret)

    def login_(self, user, secret):
        r = requests.post(self.QB_URL % "/login", data={"username": user, "password": secret})
        return dict(SID=r.cookies["SID"])

    def num_active(self):
        return len(requests.get(self.QB_URL % "/query/torrents", cookies=self.cookies).json())

    def poster(self):
        def poster_(magnet):
            requests.post(self.QB_URL % "/command/download", data=dict(urls=magnet), cookies=self.cookies)
        return poster_

class TExporter:
    """ Export torrent using Transmission"""
    def __init__(self, host, port, user, secret):
        from shifter import Client
        self.client_ = Client(host=host, port=port, username=user, password=secret)

    def num_active(self):
        return len(self.client_.list())

    def poster(self):
        def poster_(magnet):
            self.client_.add(filename=magnet)
        return poster_

def make_rule(s):
    s = s.replace("(", " ( ").replace(")", " ) ").strip()

    def mapper(s):
        if s.lower() in ("and", "or", "not"):
            return s.lower()
        elif s == ")":
            return ")"
        elif s == "(":
            return "("
        else:
            return 'M("%s")' % s

    return " ".join(map(mapper, re.split(r"\s+", s)))

def make_expr(fn):
    """
    Translate something like this:
    *-KTR and *XXX* and (*720p* or *1080p* or *2160p*)
    """
    def rule_label(x):
        a = x.split(";")
        if len(a) > 1:
            return {"rule": make_rule(a[0]), "label": a[1].strip()}
        else:
            return {"rule": make_rule(a[0]), "label": "default"}
    with open(fn, "r") as f:
        return [ rule_label(x) for x in f.read().splitlines()
                 if len(x.strip()) > 0 and not x.startswith("#") ]

def rss_get_text(el):
    rc = []
    for e in el:
        if e.nodeType == e.TEXT_NODE:
            rc.append(e.data)
    return "".join(rc).strip()

def magnet_get_dn(magnet):
    m = urlparse.urlparse(magnet)
    title = urlparse.parse_qs(m.query)["dn"][0]
    return title

def rss_process_dom(xml, add_function):
    try:
        dom = parseString(xml)
    except:
        with open("failed-xml-%s.xml" % str(time.time())) as f:
            f.write(xml)
        raise

    links = dom.getElementsByTagName("link")
    for link in links:
        magnet = rss_get_text(link.childNodes)
        if magnet.startswith("magnet:"):
            title = magnet_get_dn(magnet)
            add_function(title, magnet)

field_names = ['magnet', 'label', 'status']
def write_csv(data):
    with open(db_file + ".tmp", "w") as f:
        w = csv.DictWriter(f, fieldnames=field_names)
        w.writeheader()
        for row in data:
            w.writerow(row)
    os.rename(db_file + ".tmp", db_file)

def read_csv():
    if not os.path.exists(db_file):
        return []
    with open(db_file, "r") as f:
        r = csv.DictReader(f)
        data = []
        for row in r:
            data.append(row)
        return data

def has_magnet(data, magnet):
    for row in data:
        if magnet == row["magnet"]:
            return True
    return False

def rss_process_import(url=None, rules_file=None):
    rules = make_expr(args.rules_file)
    print("rules", rules)

    data = read_csv()

    def add_function(s, m):
        if has_magnet(data, m):
            print("- seen %s" % s)
            return False

        def M(p):
            return fnmatch.fnmatch(s.upper(), p.upper())

        label = None
        for rule in rules:
            if eval(rule["rule"]):
                label = rule["label"]

        if label is None:
            print("- no-match %s" % s)
            return False

        data.append({"magnet": m, "label": label, "status": "W"})

        print("+ added %s" % s)
        return True

    r = requests.get(url)
    rss_process_dom(r.content, add_function)

    write_csv(data)

def db_cleanup():
    data = read_csv()

    def removeall_(find=None, remove=[]):
        to_remove = []
        dns = [ magnet_get_dn(x["magnet"]) for x in data ]
        for dn in [ x for x in dns if find in x ]:
            for idx, other_dn in enumerate(dns):
                for r in remove:
                    if (r in other_dn) and dn == other_dn.replace(r, find):
                        print("% removing", other_dn, "dupe", dn)
                        to_remove.append(idx)
        for idx in sorted(to_remove, reverse=True):
            del data[idx]

    for cleanup_rule in cleanup_rules:
        removeall_(**cleanup_rule)

    write_csv(data)

def qb_process(r, nexter, poster):
    for i, next_ in zip(r, nexter()):
        poster(next_)
        print("! added torrent", i, next_)

def qb_export(exporter, max_torrents=None, take_last=None):

    num = exporter.num_active()
    poster = exporter.poster()

    data = read_csv()

    def nexter():
        for row in data:
            if row["status"] != "T":
                row["status"] = "T"
                yield row["magnet"]

    # go through it
    qb_process(range(num, max_torrents), nexter, poster)

    write_csv(data)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", dest="file_",
                        help="The file to use for list of magnets [default magnets.txt]",
                        required=True, default="magnets.txt")
    parser.add_argument("--import", dest="import_", help="Import magnet links from RSS feed", action="store_true")
    parser.add_argument("--import-url", help="RSS feed URL [default https://rarbg.com/rssdd_magnet.php]",
                        default="https://rarbg.com/rssdd_magnet.php")
    parser.add_argument("--rules-file", help="List of rules to include", default="rules.txt")
    parser.add_argument("--cleanup", help="Cleans resolution duplicates", action="store_true")
    parser.add_argument("--export", help="Export using type [qb or t]", nargs=1, metavar="TYPE")
    parser.add_argument("--export-user", help="Export client user [default admin]", default="admin")
    parser.add_argument("--export-pass", help="Export client pass [default adminadmin]", default="adminadmin")
    # qb exporter config
    parser.add_argument("--qb-url", help="qbittorrent URL [default http://127.0.0.1:8080]",
                        default="http://127.0.0.1:8080")
    # transmission exporter config
    parser.add_argument("--t-host", help="Transmission host [default localhost]",
                        default="localhost")
    parser.add_argument("--t-port", help="Transmission port [default 9091]", default=9091, type=int)
    parser.add_argument("--max-torrents", help="Max torrents to have in qbittorrent [default 100]",
                        default=100, type=int)
    parser.add_argument("--take-last", help="Take the last added link [default False]", action="store_true")
    args = parser.parse_args()

    db_file = args.file_
    if args.import_:
        rss_process_import(url=args.import_url, rules_file=args.rules_file)
    if args.cleanup:
        db_cleanup()
    if args.export:
        # build exporter
        if args.export == "qb":
            exporter = QBExporter(url=args.qb_url, user=args.export_user, secret=args.export_pass)
        elif args.export == "t":
            exporter = TExporter(host=args.t_host, port=args.t_port,
                                 user=args.export_user, secret=args.export_pass)
        else:
            raise Exception("Please use --export together with --qb-url or --t-url")
        qb_export(exporter, max_torrents=args.max_torrents, take_last=args.take_last)
